importScripts('https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js');

const firebaseConfig = {
    apiKey: "AIzaSyCkyRarJFqstY1fk-4BBIWHUxnFThLTtVo",
    authDomain: "coba-4f237.firebaseapp.com",
    projectId: "coba-4f237",
    storageBucket: "coba-4f237.appspot.com",
    messagingSenderId: "719108759555",
    appId: "1:719108759555:web:301ceaf1768f2d26d8ee36",
    measurementId: "G-ZN64EBTRF5"
  };

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.onBackgroundMessage(function (payload) {
  console.log('Pesan notifikasi Firebase:', payload);
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: payload.notification.icon,
  };
  self.registration.showNotification(notificationTitle, notificationOptions);
});
