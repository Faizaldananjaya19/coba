document.addEventListener('DOMContentLoaded', function() {
    const questionContainer = document.getElementById('question-container');
    const resultContainer = document.getElementById('result-container');
    const questionElement = document.getElementById('question');
    const choicesElement = document.getElementById('choices');
    const resultElement = document.getElementById('result');
    const submitButton = document.getElementById('submit-btn');

    let currentQuestionIndex = 0;
    let score = 0;

    fetch('questions.json')
      .then(response => response.json())
      .then(data => {
        const questions = data;
        showQuestion();
        submitButton.addEventListener('click', () => {
          const selectedChoice = document.querySelector('input[name="choice"]:checked');
  
          if (selectedChoice) {
            const selectedAnswer = selectedChoice.value;           
            if (selectedAnswer === questions[currentQuestionIndex].correctAnswer) {
              score++;
            }
            currentQuestionIndex++;
            if (currentQuestionIndex < questions.length) {
              showQuestion();
            } else {
              showResult();
            }
          }
        });

        function showQuestion() {
          const question = questions[currentQuestionIndex];
          questionElement.textContent = question.question;
          while (choicesElement.firstChild) {
            choicesElement.firstChild.remove();
          }
  
          function generateChart() {
            const chartData = {
                labels: ['Pertanyaan Benar', 'Pertanyaan Salah', 'Pertanyaan Kosong'],
                datasets: [
                    {
                        label: 'Hasil Tes',
                        data: [7, 2, 1],
                        backgroundColor: 'rgba(75, 192, 192, 0.4)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        pointBackgroundColor: 'rgba(75, 192, 192, 1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(75, 192, 192, 1)'
                    }
                ]
            };
        
            const chartOptions = {
                scales: {
                    r: {
                        beginAtZero: true,
                        max: 10
                    }
                }
            };
        
            const chartElement = document.getElementById('chart');
            new Chart(chartElement, {
                type: 'radar',
                data: chartData,
                options: chartOptions
            });
        }
        function showResult() {
            questionContainer.style.display = 'none';
            resultContainer.style.display = 'block';
            const percentage = (score / questions.length) * 100;
            resultElement.textContent = `Nilai Anda: ${percentage.toFixed(2)}%`;
        
            generateChart();
        }

          question.choices.forEach(choice => {
            const li = document.createElement('li');
            const input = document.createElement('input');
            input.setAttribute('type', 'radio');
            input.setAttribute('name', 'choice');
            input.setAttribute('value', choice);
  
            const label = document.createElement('label');
            label.textContent = choice;
  
            li.appendChild(input);
            li.appendChild(label);
            choicesElement.appendChild(li);
          });
        }
        
        function showResult() {
          questionContainer.style.display = 'none';
          resultContainer.style.display = 'block';
          const percentage = (score / questions.length) * 100;
          resultElement.textContent = `Nilai Anda: ${percentage.toFixed(2)}%`;
        }
      })
      .catch(error => {
        console.error('Error:', error);
      });
  });